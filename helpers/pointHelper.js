
var pointHelper = function()
{
    /**
    * @description  Calculate the slope between two points. If the points have the same value for x or y it will be returned the string 'horizontal' or 'vertical'
    * @param        {Point} pointA
    * @param        {Point} pointB
    * @returns      {string/number} 
    */
    var slope = function (pointA, pointB)
    {
        return pointB.y === pointA.y ? 'horizontal' : pointB.x === pointA.x ? "vertical" : ((pointB.y - pointA.y) / (pointB.x - pointA.x));
    };
    /**
    * @description  Use to initializate the Point object
    * @param        {int} x
    * @param        {int} y
    */
    var Point = function(x, y)
    {
        this.x = x;
        this.y = y
    };

    return{
        /**
        * @description  Order the points from the smallest to the biggest
        * @param        {array<Point>} points
        * @returns      {array<Point>}
        */
        orderPoints: function(points)
        {

            var len  = points.length,
                i    = 0;
            
               for(; i<len-1; i++) 
                { 
                    var min = i,
                        j   = i+1,
                        temp;
        
                    for(; j < len; j++ ) 
                    {
                        if( points[j].x < points[min].x || (points[j].x === points[min].x && points[j].y < points[min].y))
                            min = j;
                    }
        
                    temp        = points[min]; 
                    points[min] = points[i]; 
                    points[i]   = temp; 
                }
        
            return points;
        },
        /**
        * @description  Starting from a point, get as "origin" of the line, calculate the slope between it and each other point. If the slope is the same it means that they belong to the same line.
        * @param        {array<Point>} points
        * @returns      {hashmap<(x;y), array<point>>}
        */
        findPointsBelongToTheSameLine: function(points)
        {

                var hashmap  = {};
                var i       = 0;
                var len1    = points.length;         
                for(; i<len1; i++)
                {   
                    var originPoint = points[i].x + ";" + points[i].y;
                    var z           = i + 1; 

                    for(; z<len1; z++)
                    {  
                        var slopeValue = slope(points[i], points[z]);
                        if(!hashmap.hasOwnProperty(originPoint)) hashmap[originPoint] = { points: [], slope: slopeValue };
                        if(hashmap[originPoint].points.indexOf(points[i]) < 0) hashmap[originPoint].points.push(points[i]);
                        if(hashmap[originPoint].points.indexOf(points[z]) < 0 && hashmap[originPoint].slope === slopeValue) hashmap[originPoint].points.push(points[z]);
                    }
                }
                return hashmap;
        },
        /**
        * @description  Find the points that belong to a line
        * @param        {Object{ "x;y": string, points: array<point>, slope: int }} lines
        * @param        {array<Point>} points
        * @returns      {array<Object{ points: array<point>, slope: int }>}
        */
        checkMatchBetweenLinesAndPoints: function(lines, points)
        {
            var ret = [];
            for(var key in lines)
            {
                var tmp         = key.split(";");
                var pointA      = new Point(tmp[0], tmp[1]);
                for(var i = 0; i < points.length; i++)
                {
                    //it's the same point
                    if(pointA.x === points[i].x && pointA.y === points[i].y)
                    {
                        ret.push(lines[key])
                    }
                    //check the slope
                    else
                    {
                        var slopeValue  = slope(pointA, points[i]);
                        if(lines[key].slope === slopeValue && ret.indexOf(lines[key]) < 0)
                        {
                            ret.push(lines[key])
                        }
                    }
                }
            }
            return ret;
        },
        /**
        * @description  Check if the point has the properties x and y
        * @param        {Point} point
        * @returns      {Boolean} 
        */
        hasPointProperties: function(point) 
        {
            return !!point && point.x && point.y && Number.isInteger(point.x) && Number.isInteger(point.y)
        },
        /**
        * @description  Remap array of string to a Point
        * @param        {Array<string>} points
        * @returns      {Array<Object{ status: int, points: array<Point> }>} 
        */
        parameterToPoint: function(points)
        {
            var formattedPoints = {
                status: 200,
                points: []
            };
            if(!!points && points.length)
            {
                var i = 0,
                    len = points.length;

                for(; i < len; i++)
                {
                    var tmp = points[i].split(";");
                    if(tmp && tmp.length === 2)
                    formattedPoints.points.push(new Point(tmp[0], tmp[1]));
                    else 
                    {
                        formattedPoints.status = 500;
                        break;
                    }
                }
            }
            return formattedPoints;
        }
    }
}

module.exports = pointHelper;