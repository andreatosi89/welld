var config = {
    port: '3000',
    numberOfPointsPerLine: 2,
    mysql: {
        host     : 'localhost',
        user     : 'root',
        password : 'root',
        database : 'welld'
    }
};

module.exports = config;