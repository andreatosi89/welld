var points = require('./../controllers/points');

var routes = function (app) {
    
    //Given a set of N feature points in the plane, determine every line that contains N or more of the points, and return all points involved
    app.post('/',            points.findLines);
    //Get all points in the space
    app.get('/space',       points.getPoints);
    //Get all line segments passing through at least N points. Note that a line segment should be a set of points.
    app.get('/lines/:points',   points.getLinesHavingCommonPoints);
    //Add a point to the space
    app.post('/point',      points.addPoint);
    //Remove all points from the space
    app.delete('/space',    points.truncatePoints);
    
};

module.exports = routes;