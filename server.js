var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var config      = require('./config');

app.use(bodyParser.json());
app.set('port', config.port);

require('./routes')(app);

app.settings.port = app.settings.port || '3000';
app.listen(app.settings.port);
console.log("Listening on port " + app.settings.port + "...");