CREATE TABLE `points` (
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


insert into points (x,y) values (1,1);
insert into points (x,y) values (2,2);
insert into points (x,y) values (4,5);
insert into points (x,y) values (6,4);
insert into points (x,y) values (5,6);
insert into points (x,y) values (2,8);