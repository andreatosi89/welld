CONSIDERATIONS

# Each point has infinity lines passing through it.
# Every couple of points has a line passing through them.

//-----------------------------------------------------------------------------------------------------

Get all line segments passing through at least N points. Note that a line segment should be a set of
points. => For each point given, I will check if it passes through a line;