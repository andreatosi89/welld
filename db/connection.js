var mysql       = require('mysql');

var connection = function(config){

//Create db connection
var conn = mysql.createConnection({
    host     : config.mysql.host,
    user     : config.mysql.user,
    password : config.mysql.password,
    database : config.mysql.database
  })


  return {
    executeQuery: function(query, callback){
      conn.connect();
      conn.query(query, callback);
      conn.end();
    }
  } 

}
module.exports = connection;