var config      = require('./../config');
var connection  = require('./../db/connection');
var helper      = require('./../helpers/pointHelper');

var points = 
{
    findLines: function (req, res) 
    {
        if(config.numberOfPointsPerLine)
        {
            var _points = req.body;
            //Determine every line that contains " + config.numberOfPointsPerLine + " or more of the points, and return all points involved.
            if(!!config.numberOfPointsPerLine && !Number.isInteger(config.numberOfPointsPerLine))
                res.status(500).send("The number variable must be an integer");
            else if(config.numberOfPointsPerLine > _points.length)
                res.status(500).send("The number of points is less than the number of how many points should belong to a line");
            else 
            {
                for(var k = 0, len = _points.length, wrongFormat = false; k < len; k++)
                {
                    if(!helper().hasPointProperties(_points[k]))
                    {
                        wrongFormat = true;
                        break;       
                    }
                }
                if(!wrongFormat)
                {
                    var ret             = { message: "Lines found that match with at least " + config.numberOfPointsPerLine + " given points"};
                    var orderedPoints   = helper().orderPoints(_points);
                    var mappedPoint     = helper().findPointsBelongToTheSameLine(orderedPoints);
                    var i               = 0;

                    for(var key in mappedPoint)
                    {
                        if(mappedPoint[key].points.length >= config.numberOfPointsPerLine)
                        {
                            ret["Line " + i] = mappedPoint[key].points;
                            i++;
                        }
                    }
                    res.send(i > 0 ? ret : "No lines with at least " + config.numberOfPointsPerLine + " points have been matched");
                }
                else 
                    res.status(500).send("Point format not complaint. It must have X and Y properties as an integer")
            }
        }
        else
            res.status(500).send("Please set how many points should belong to a line");
    },
    addPoint: function (req, res) 
    {
        if(helper().hasPointProperties(req.body))
        {
            connection(config).executeQuery('insert into welld.points (x,y) values ('+ req.body.x +', '+ req.body.y +');', function(err, rows, fields) {
                if (!err) res.status(200).send('Entity created');
                else res.status(500).send('Error while performing Query.' + err); 
            });
            
        } 
        else res.status(500).send("Point format not complaint. It must have X and Y properties as an integer");
    },
    getPoints: function (req, res) 
    {
        connection(config).executeQuery('SELECT * from points', function(err, rows, fields) {
            var ret = { message: "All existing points", points: rows };
            if (!err) res.status(200).send(ret); 
            else  res.status(500).send('Error while performing Query.' + err); 
        });
    },
    getLinesHavingCommonPoints: function (req, res) 
    {
        var params = req.params && req.params.points ? req.params.points.split("&") : null;
        if(params)
        {
            var pointsParamenter = helper().parameterToPoint(params);
            if(pointsParamenter.status === 200)
            {
                connection(config).executeQuery('SELECT * from points', function(err, rows, fields) {
                    if (!err)
                    {
                        var orderedPoints   = helper().orderPoints(rows);
                        var mappedPoint     = helper().findPointsBelongToTheSameLine(orderedPoints);
                        //For each point given check if it passes through a line. If it does the result will be displayed;
                        var result          = helper().checkMatchBetweenLinesAndPoints(mappedPoint, pointsParamenter.points);
                        var ret = { message: "All lines matched with given points", points: result };
                        res.status(200).send(ret); 
                    }
                    else  res.status(500).send('Error while performing Query.' + err); 
                });
            }
            else
                res.status(500).send('Invalid format for parameter. It must be X1;Y1&X2;X3...'); 
        }
        else
        {
            res.status(500).send('Missing parameters'); 
        }
    },
    
    truncatePoints: function()
    {
        connection(config).executeQuery('TRUNCATE points', function(err, rows, fields) {
            if (!err) res.status(200).send('Entities removed'); 
            else  res.status(500).send('Error while performing Query.' + err); 
        });
    }
};

module.exports = points;